chrome.browserAction.onClicked.addListener(function(tab) {
    
    chrome.tabs.query({}, function(tabs){

        var text = '';

        for(var i = 0; i < tabs.length; i++){

            if(i !== 0){
                text += '\r\n';
            }

            text += tabs[i].title+'\r\n';
            text += tabs[i].url+'\r\n';
        }

        chrome.downloads.download({
            url: URL.createObjectURL(new Blob([text], {type : 'application/txt'})),
            filename: 'my_opened_tabs.txt'
        });

    });
});